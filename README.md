# Hugo Documentation server for Infra

This docker-compose configuration installs Traefik and a pre-fabricted container _with_ current content.
For the Museum authentication we use _Google Authentication_ . After every commit in the museum-documentation, content validity will be tested, and a new container with web-content will be deployed.

# Deployment

```bash
cd gii/docs/ansible-docs
ansible-galaxy install -r ./requirements.yml --roles-path ./roles/
ansible-galaxy collection install community.general
# Known error, see below
ansible-playbook  playbooks/deploy.yml -l docs-infra-prd-001 
```
## Extra actions
Apart from Traefik and Route53 actions:
- creat a gitlab read-only-token to the registry
-- docker login registry.gitlab.com 
- install a crontab
```bash
PATH=/bin:/usr/bin:/usr/sbin:/usr/local/bin
*/15 * * * * (cd /opt/composeproject; docker-compose pull; docker-compose up -d)
```
Both taken care of by ansible.

# Flow after new content is commited.
- After a commit in the core/infra-documentation repo, a gitlab-runner will stage a test for hugo-content validity.
- If this succeeds, a container will be will be build containing hugo-converted web-content.
- This container will be published to the registry.
- A crontab will check with a 'docker-compose pull' if a new container is availble
- If so, this one will be pulled and replace the running one.

# Authentication / Authorization
To protect our documentation we need some authorization. Best would bij Google-Naturalis authentication limited to Infra members. This is ToDo.
For now we go for the simple 'Basic Authentication' with the user _ictdocs_. See Bitwarden

# Known Error
## docker login fails
When installing the server, docker-compose needs to login into a private registry. So we have a task _before_ the docker-compose-role that does exactly this. But if will fail because the tools to do so are installed with the docker-compose-role that comes later....
Fix: First run ansible with the tag _docker-compose_. Than run the whole playbook.

